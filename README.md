# Lisp

## [Clojure](http://clojure.org/)

### [Clojars](https://clojars.org/)

### [Leiningen](http://leiningen.org/)-:-:-[SOURCE](https://github.com/technomancy/leiningen)

* [使用leiningen搭建clojure开发环境](http://www.blogjava.net/javalinjx/archive/2013/07/03/401170.html)
* [Leiningen教程中文版](http://wiki.fnil.net/index.php?title=Leiningen_tutorial%E4%B8%AD%E6%96%87%E7%89%88)

### [LightTable](http://www.lighttable.com/)-:-:-[SOURCE](https://github.com/LightTable/LightTable)

### [SBCL](http://sbcl.org/)-:-:-[SOURCE](https://github.com/sbcl/sbcl)

* [中文资料](http://lisp.org.cn/wiki/lisp/common-lisp/sbcl)

## [Haskell](http://www.haskell.org/)

* [十分钟学会 Haskell](http://www.haskell.org/haskellwiki/Cn/%E5%8D%81%E5%88%86%E9%92%9F%E5%AD%A6%E4%BC%9A_Haskell)
* [Real World Haskell 中文版](http://rwh.readthedocs.org/en/latest/index.html)
* [Haskell趣學指南](http://learnyouahaskell-zh-tw.csie.org/zh-cn/chapters.html)

### [The Haskell Platform](http://www.haskell.org/platform/)
### [Leksah](http://leksah.org/)

## [Racket](http://racket-lang.org/)